﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace 五味子串口调试助手3
{
    public class OnOffPicSourceConverterConverter : IValueConverter
    {
        public object Convert(object i_Value, Type i_TargetType, object i_Parameter, CultureInfo i_Culture)
        {
            var binding = i_Value;
            if (binding != null)
            {
                return (bool)binding ? "/五味子串口调试助手3;component/Resources/on.png" : "/五味子串口调试助手3;component/Resources/off.png";
            }
            return null;
        }

        public object ConvertBack(object i_Value, Type i_TargetType, object i_Parameter, CultureInfo i_Culture)
        {
            throw new NotImplementedException();
        }
    }
}
