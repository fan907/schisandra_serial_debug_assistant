﻿
using System.Windows.Input;
namespace 五味子串口调试助手3
{
    public class addConditionSendCommand : ICommand
    {
        public MainWindowModel ViewModel { get; set; }

        public addConditionSendCommand(MainWindowModel ViewModel)
        {
            this.ViewModel = ViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            ViewModel.list_conditionSend.Add(new conditionSendDataClass());
        }

        public event System.EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
