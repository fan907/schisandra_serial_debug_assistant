﻿

using System.Windows.Input;
namespace 五味子串口调试助手3
{
    public class addClcSendCommand : ICommand
    {
        public RoutedCommand cmd { get; set; }
        public MainWindowModel ViewModel { get; set; }

        public addClcSendCommand(MainWindowModel ViewModel)
        {
            this.ViewModel = ViewModel;
            cmd = new RoutedCommand();

        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            ViewModel.list_clcSend.Add(new clcSendDataClass());
        }

        public event System.EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
