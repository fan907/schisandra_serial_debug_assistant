﻿using System;

namespace 五味子串口调试助手3
{
    public class CheckSum16 : checkBase
    {
        public override string checkData(string s)
        {
            byte[] array = tools.hexStringToByte(s);
            return tools.bytesToHexString(BitConverter.GetBytes(this._CheckSum16(array, array.Length)));
        }

        private ushort _CheckSum16(byte[] buf, int len)
        {
            ushort num = 0;
            for (int i = 0; i < len; i++)
            {
                num += (ushort)buf[i];
            }
            return num;
        }

        public override string ToString()
        {
            return "校验和-16";
        }
    }
}
